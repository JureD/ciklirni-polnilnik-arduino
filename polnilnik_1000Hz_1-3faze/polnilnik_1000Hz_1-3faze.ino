int phase = 0;

void setup() {
  pinMode(8, OUTPUT);

  cli();//stop interrupts
  //set timer0 interrupt at 2kHz
  TCCR0A = 0;// set entire TCCR0A register to 0
  TCCR0B = 0;// same for TCCR0B
  TCNT0  = 0;//initialize counter value to 0
  // set compare match register for 3khz increments
  OCR0A = 82;// = (16*10^6) / (2000*64) - 1 (must be <256)
  // turn on CTC mode
  TCCR0A |= (1 << WGM01);
  // Set CS01 and CS00 bits for 64 prescaler
  TCCR0B |= (1 << CS01) | (1 << CS00);   
  // enable timer compare interrupt
  TIMSK0 |= (1 << OCIE0A);

  sei(); //allow interrupts

}

void loop() {
  // put your main code here, to run repeatedly:

}

ISR(TIMER0_COMPA_vect){
  phase += 1;
  digitalWrite(8, LOW);
  if (phase == 3){
    digitalWrite(8, HIGH);
    phase = 0;
  }
  //else{
    //digitalWrite(8, LOW);
  //}
}
