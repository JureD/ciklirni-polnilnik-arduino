unsigned long previousMicros = 0;
const long interval = 10;
int myArray[100];

void setup() {
  Serial.begin(9600);
  
}
void loop() {
  for (int counter = 0; counter < 100; counter++) {
    unsigned long currentMicros = micros();
    if (currentMicros - previousMicros >= interval) {
      previousMicros = currentMicros;
      myArray[counter] = analogRead(A2);
    }
  }
  Serial.println("Data:");
  for (int i = 0; i < 100; i++) {
    Serial.println(myArray[i]);
  }
}
